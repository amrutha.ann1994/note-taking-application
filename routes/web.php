<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/notes/data', [App\Http\Controllers\HomeController::class, 'getNotes'])->name('notes');
Route::get('/note/add', [App\Http\Controllers\HomeController::class, 'addNote']);
Route::post('/note/save', [App\Http\Controllers\HomeController::class, 'addNote']);
Route::get('/note/edit/{id}', [App\Http\Controllers\HomeController::class, 'editNote']);
Route::post('/note/update', [App\Http\Controllers\HomeController::class, 'editNote']);
Route::get('/note/delete/{id}', [App\Http\Controllers\HomeController::class, 'deleteNote']);
Route::get('/note/view/{id}', [App\Http\Controllers\HomeController::class, 'viewNote']);