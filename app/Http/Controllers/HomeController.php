<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Yajra\Datatables\Datatables;
use Log;
use App\Models\Note;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index','getNotes','addNote']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('notes');
    }
    public function getNotes()
    {
        // Log::info("in index",[$request->ajax()]);
        // Log::info("in index1111");
        $data = Note::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->make(true);

    }

    public function addNote(Request $request)
    {
        if ($request->title) {
            // Log::info("Request - ",$request->except('_token'));
            Note::create($request->except('_token'));
            return redirect()->route('home');

        }else{
            return view('add');
        }
        
    }
     public function editNote(Request $request)
    {
        if ($request->title) {
            // Log::info("Request - ",$request->except(['_token','id']));
            $note = Note::where('id',$request->id)->update($request->except(['_token','id']));
            return redirect()->route('home');
        }else{
            // Log::info($request->id);
            $note = Note::find($request->id);
            // Log::info("Notee",[$note]);
            return view('edit')->with('note',$note);
        }
        
    }

    public function deleteNote(Request $request)
    {
        if ($request->id) {
            // Log::info("Request - ",$request->except('_token'));
            Note::where('id',$request->id)->delete();
            return true;

        }else{
            return false;
        }
        
    }

    public function viewNote(Request $request)
    {
        // Log::info($request->id);
        $note = Note::find($request->id);
        // Log::info("Notee",[$note]);
        return view('view')->with('note',$note);
        
    }

}
