<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {!! HTML::style('assets/css/icons/icomoon/styles.css') !!}
    {!! HTML::style('assets/css/icons/fontawesome/styles.min.css') !!}
    {!! HTML::style( 'assets/css/bootstrap.css') !!}
</head>
<body>
    <div id="app">

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <div class="row" style="flex-wrap: inherit;">
                                    <div class="col-md-6">
                                        <h3>{{ __($note->title) }}</h3></div>
                                    <div class="col-md-6">
                                            <a id="cancel" name="cancel" class="btn float-right"><i class="fa fa-close"></i></a>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="card-body">
                                <div class="row">
                                  <div class="col-md-12">
                                      <p>{{ __($note->note) }}</p>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </main>
    </div>
</body>
{!! HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js') !!}
<script >
     $(document).ready(function(){
        $("#cancel").click(function() {
            window.parent.closeModal();
        });
    });
</script>
</html>