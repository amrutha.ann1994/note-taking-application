@extends('layouts.app')
@section('style')
{!! HTML::style('https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css') !!}
{!! HTML::style('https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css') !!}
{!! HTML::style('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') !!}
{!! HTML::style('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') !!}
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">{{ __('Notes') }}</div>
                        <div class="col-md-6"><a href="{{asset('note/add')}}" class="btn btn-primary float-right"><i class="icon-pen"></i> Add New Note</a></div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered data-table" id="notes-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th width="100px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<iframe id="ajax-modal" class="modal modal-ajax" tabindex="-1" style="display: none;"></iframe>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer></script>
<script >
   $(document).ready(function(){
    console.log('test')
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
         ajax: "{{asset('notes/data')}}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'title', name: 'title'},
            {data: null, name: 'action',render: function ( data, type, row ) {
                var editPath = "{{asset('note/edit')}}/"+data.id
                var viewfield = '<button id="view" class=" btn btn-sm btn-grey" data-id="'+data.id+'" ><i class="icon-eye icon-white"></i></button>';
                var editfield = '<a id="edit" href="'+editPath+'" class="editor_edit btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>';
                var deletefield = '<button id="delete" class=" btn btn-sm btn-danger" data-id="'+data.id+'" ><i class="fa fa-trash"></i></button>';
                return  viewfield + ' '+editfield + ' ' + deletefield;
                }
            }
        ]
    });

    $('#notes-table').on('click', '#delete', function (e) {
        var id = $(this).attr('data-id');
        var result =  confirm("Are you sure you want to delete this?");
          if (result) {
            console.log(result);
              $.ajax({
               url:"{{asset('note/delete')}}/"+id,
               type:'GET',
               success:function(){
                 table.ajax.reload(null, false);
               }
            })
          }else{
            console.log(result);
          };
    });
    
    $modal = $('#ajax-modal');
    $(document).on("click", "#view", function() {
        var noteId = $(this).attr('data-id');

        setTimeout(function() {
            $modal.attr('src', '{{asset("note/view")}}/'+ noteId, function() {
            });
            $modal.modal();
        });
    });
  });
  function closeModal() {
        $("#ajax-modal").modal('hide');
    }
</script>
@endpush