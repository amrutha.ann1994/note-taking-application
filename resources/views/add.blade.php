@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">{{ __('Add Notes Here') }}</div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            {!!  Form::open(array('url'=>'note/save','id'=>'addnote','method'=>'POST', 'class' => 'form-horizontal'))  !!}
                               <div class="panel panel-default">
                                  <div class="panel-body">
                                     <fieldset>
                                        <div class="row">
                                           <div class="col-md-12">
                                              @foreach($errors->all() as $error)
                                              <div class="errorHandler alert alert-danger">
                                                 <i class="icon-remove-sign"></i> {!!$error!!}
                                              </div>
                                              @endforeach
                                           </div>
                                               <div class="col-md-12 ">
                                                  <div class="form-group has-feedback has-feedback-left">
                                                      {!! Form::text('title','',['id'=>'title','class'=>'form-control','placeholder'=>'Title','required'=>'required']) !!}
                                                </div>
                                                  <div class="form-group has-feedback has-feedback-left">

                                                {!! Form::textarea('note','',['id'=>'note','class'=>'form-control','placeholder'=>'Notes here','required'=>'required']) !!}
                                                </div>
                                               </div>
                                        </div>
                                        <div class="row">
                                           <div class="col-md-3" align="center" style="padding-top: 15px;">
                                              {!!Form::submit('Submit',['class'=>'btn bg-teal-400 btn-primary'])!!}
                                           </div>
                                        </div>
                                     </fieldset>
                                  </div>
                               </div>
                               {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script >

</script>
@endpush